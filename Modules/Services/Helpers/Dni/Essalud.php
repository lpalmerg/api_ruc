<?php

namespace Modules\Services\Helpers\Dni;

use Modules\Services\Utilities\Functions;
use Modules\Services\Utilities\Models\Person;
use GuzzleHttp\Client;

class Essalud
{
    public static function search($number)
    {
        if (strlen($number) !== 8) {
            return [
                'success' => false,
                'message' => 'DNI tiene 8 digitos.'
            ];
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://ww1.essalud.gob.pe/sisep/postulante/postulante/postulante_obtenerDatosPostulante.htm?strDni={$number}");        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);         
        curl_close ($ch);
 

        if ($response) {

            $json = (object) json_decode($response, true);
            $data_person = $json->DatosPerson[0];
            if (isset($data_person) && count($data_person) > 0 &&
                strlen($data_person['DNI']) >= 8 && $data_person['Nombres'] !== '') {
                $person = new Person();
                $person->nombres_apellidos = $data_person['ApellidoPaterno'].' '.$data_person['ApellidoMaterno'].', '.$data_person['Nombres'];
                $person->numero = $data_person['DNI'];
                $person->codigo_verificacion = Functions::verificationCode($data_person['DNI']);
                $person->primer_apellido = $data_person['ApellidoPaterno'];
                $person->segundo_apellido = $data_person['ApellidoMaterno'];
                $person->nombres = $data_person['Nombres'];
                $person->fecha_de_nacimiento = $data_person['FechaNacimiento'];
                $person->sexo = ((string)$data_person['Sexo'] === '2')?'Masculino':'Femenino';

                return [
                    'success' => true,
                    'data' => $person
                ];
            } else {
                return [
                    'success' => false,
                    'message' => 'Datos no encontrados.'
                ];
            }
        }

        return [
            'success' => false,
            'message' => 'Conexión fallida.'
        ];
    }
}
?>